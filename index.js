/**
 * This bot illustrates how to use the CaféX Live Assist for Dynamics 365
 * bot escalation API. To use the bot, first configure the LA_ACCOUNT variable
 * with the ID number of your Live Assist instance. This may be found in Dynamics
 * by navigating to: Settings > Live Assist > Account Number.
 *
 * Once configured, simply launch the bot, and when you wish to escalate to a Live
 * Assist agent, type the text: help. 
 *
 * Optionally, you may set the skill that the bot will attempt to route to via the
 * LA_SKILL variable.
 */

 "use strict";

// Settings
const LA_ACCOUNT = 'YOUR_ACCOUNT_HERE';
const LA_SKILL = 'enter-a-live-assist-agent-skill-here';

var builder = require("botbuilder");
var botbuilder_azure = require("botbuilder-azure");
var path = require('path');

// To use the bot SDK, you must have npm installed the liveassist-botsdk-js
const liveAssist = require('@cafex/liveassist-botsdk-js');

var useEmulator = (process.env.NODE_ENV == 'development');

var connector = useEmulator ? new builder.ChatConnector() : new botbuilder_azure.BotServiceConnector({
	appId: process.env['MicrosoftAppId'],
	appPassword: process.env['MicrosoftAppPassword'],
	stateEndpoint: process.env['BotStateEndpoint'],
	openIdMetadata: process.env['BotOpenIdMetadata']
});

var bot = new builder.UniversalBot(connector);
bot.localePath(path.join(__dirname, './locale'));

const MODE = {
	BOT: 0,
	ESC_INITIATED: 1,
	ESC_WAITING: 2,
	ESC_CHATTING: 3,
};

let chatData;

function removeChatData() {
	chatData = undefined;
}

function getChatData(session) {
	if (!chatData) {
		chatData = {
			visitorAddress: session.message.address,
			mode: MODE.BOT
		};
	}
	return chatData;
}

bot.dialog('/', function(session) {
	let chatData = getChatData(session);
	switch (chatData.mode) {
		case MODE.BOT:

			if (/^help/i.test(session.message.text)) {
				session.beginDialog('/escalateQuery');
			} else {
				let visitorText = session.message.text;
				let botText = 'You said: "' + visitorText + '"';
				session.send(botText, session.message.text);
			}

			break;

		case MODE.ESC_INITIATED:
			session.send('Please wait, I\'m trying to connect you to an agent');
			break;

		case MODE.ESC_WAITING:
			session.send('Please wait, waiting for an agent');
			break;

		case MODE.ESC_CHATTING:
			if (/^stop/i.test(session.message.text)) {
				chatData.escalatedChat.endChat((err) => {
					if (err) {
						session.endConversation('A problem has occurred, starting over');
						removeChatData();
					} else {
						chatData.mode = MODE.BOT;
					}
				});
			} else {
				chatData.escalatedChat.addLine(session.message.text, (err) => {
					if (err) {
						session.send('A problem has occurred sending that');
					}
				});
			}
			break;
		default:
	}
});

bot.dialog('/escalateQuery', [
	function(session) {
		session.send('Please wait while I connect you to an agent');
		let spec = {
			skill: LA_SKILL,
		};
		chatData.escalatedChat = new liveAssist.Chat(LA_ACCOUNT);
		chatData.escalatedChat.requestChat(spec, (err) => {
			if (err) {
				session.send('Sorry, I failed to contact an agent');
				chatData.mode = MODE.BOT;
			} else {
				chatData.mode = MODE.ESC_INITIATED;
				pollChat();
			}
		});
		session.endDialog();
	}
]);

function pollChat() {
	chatData.escalatedChat.poll((err, result) => {
		let endRead = false;
		if (err) {
			console.error('Error during poll: %s', err.message);
		} else {
			endRead = processEvents(result.events);
		}
		if (!endRead) setTimeout(() => pollChat(), 500);
	});
}

function processEvents(events) {
	let endRead = false;
	events.forEach((event) => {
		switch (event.type) {
			case 'state':
				switch (event.state) {
					case 'waiting':
						chatData.mode = MODE.ESC_WAITING;
						break;

					case 'chatting':
						chatData.mode = MODE.ESC_CHATTING;
						break;

					case 'ended':
						endRead = true;
						bot.beginDialog(chatData.visitorAddress, '*:/endit', chatData);
						break;

					default:
						break;
				}
				break;

			case 'line':
				if (event.source !== 'visitor') {
					let msg = event.text;
					sendProactiveMessage(chatData.visitorAddress, msg);
				}
				break;

			default:
				break;
		}
	});

	return endRead;
}

function sendProactiveMessage(address, text) {
	var msg = new builder.Message().address(address);
	msg.text(text);
	bot.send(msg);
}

bot.dialog('/endit', [
	function(session) {
		session.endConversation('Agent interaction has ended');
		removeChatData();
	}
]);

if (useEmulator) {
	var restify = require('restify');
	var server = restify.createServer();
	server.listen(3978, function() {
		console.log('test bot endpont at http://localhost:3978/api/messages');
	});
	server.post('/api/messages', connector.listen());    
} else {
	module.exports = { default: connector.listen() }
}